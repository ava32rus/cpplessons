﻿#include <iostream>
#include <ctime>

using namespace std;


int main()
{
    const int x=10;
    const int y=10;
    
    int arr[x][y];

    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            arr[i][j] = rand() % 10;
        }
    }

    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < y; j++)
        {
            cout << arr[i][j] << "\t";
        }
        cout << endl;
    }
}
